import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
//import FormsModule  to use ngModel for 2-way data-binding.
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { MatTableModule, MatSortModule } from '@angular/material';
import { MatPaginatorModule } from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {  MatButtonModule,
          MatFormFieldModule,
          MatInputModule,
          MatRippleModule,
        } from  '@angular/material';
import { AppRoutingModule } from './app-routing.module';
import { MatDialogModule } from '@angular/material/dialog';

import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { BodyComponent } from './body/body.component';
import { InputComponent } from './body/input/input.component';
import { ViewComponent } from './body/view/view.component';

import { TwoMovingService } from './service/two-moving-service.service';
import { DatePipe } from '@angular/common';

import 'hammerjs';
import { PopupComponent } from './body/view/popup/popup.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    BodyComponent,
    InputComponent,
    ViewComponent,
    PopupComponent,
  ],

  entryComponents: [PopupComponent],

  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    MatTableModule,
    MatPaginatorModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    MatRippleModule,
    MatDialogModule,
    MatSortModule
  ],
  providers: [TwoMovingService, DatePipe],
  bootstrap: [AppComponent]
})
export class AppModule { }
