import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { TwoMovingService } from '../../service/two-moving-service.service';
import { twoMoving } from 'src/app/model/twoMoving';
import { ViewComponent } from '../view/view.component';


@Component({
  providers:[ViewComponent],
  selector: 'app-input',
  templateUrl: './input.component.html',
  styleUrls: ['./input.component.css']
})
export class InputComponent implements OnInit {

  input: twoMoving;
  selectedStrategy: string;
  createStrategy: FormGroup;
  stockMap = new Map();
  forbiddenChar = ["."];

  constructor(private ts : TwoMovingService,
              private vc: ViewComponent ) { }

  ngOnInit() {
    this.createStrategy = new FormGroup ({
      //'strategy': new FormControl('Two-moving average'),
      'ticker': new FormControl('APPL'),
      'size': new FormControl(null, [Validators.required, this.positive.bind(this)]),
      'exitProfitLoss': new FormControl(null, [Validators.required, this.positive.bind(this)])
    });
    this.stockMap.set("APPL", 1);
    this.stockMap.set("BRK-A", 2);
    this.stockMap.set("GOOG", 3);
    this.stockMap.set("MSFT", 4);
    this.stockMap.set("NSC", 5);
  }

  onSubmit()  { 
  // console.log(this.createStrategy.value);
   this.input = {
     "stock": {
       "id": this.stockMap.get(this.createStrategy.value.ticker),
       "ticker": this.createStrategy.value.ticker
     },
     "size": this.createStrategy.value.size,
     "exitProfitLoss": this.createStrategy.value.exitProfitLoss
   }
   this.createStrategy.reset();
   this.createStrategy.patchValue({
     ticker: 'APPL',
     size: null,
     exitProfitLoss: null
     // other controller names goes here
   });
  this.ts.createStrategy(this.input).subscribe(data => {
    //this.vc.getAllTradeInfo();
    alert("You just created a new strategy. It will show up on the table soon.");
   // console.log("create strategy after getALl");
  }, error => {
    console.log("we got errors: " + error);
    alert("Failed to create a strategy.");
  })
  }
  
  positive(control: FormControl): {[s: string]: boolean} {
      if (control.value <= 0) {
        return {'invalid': true};
      }
    
      return null;
  }


}
