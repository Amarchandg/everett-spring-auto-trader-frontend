import { AbstractControl, ValidatorFn } from '@angular/forms';

export function badNum(numRe: RegExp): ValidatorFn {
    return (control: AbstractControl): {[key: string]: any} | null => {
      const forbidden = numRe.test(control.value);
      return forbidden ? {'forbiddenName': {value: control.value}} : null;
    };
  }