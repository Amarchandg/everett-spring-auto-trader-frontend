import { Component, OnInit, ViewChild, Input, Output, EventEmitter, Inject } from '@angular/core';

import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { TradeInfo } from 'src/app/model/tradeInfo';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import {MatSort} from '@angular/material/sort';
import { GetTradeInfoService } from 'src/app/service/get-trade-info.service';

@Component({
  selector: 'app-popup',
  templateUrl: './popup.component.html',
  styleUrls: ['./popup.component.css']
})
export class PopupComponent implements OnInit {
 // @Input() tradeInfo: TradeInfo[];
  displayedColumns: string[] = ['price', 'size', 'lastStateChange', 'trade type', 'state'];
 //tradeInfos: any[] = [
  //  {id: '1', stockName: 'apl', strategy: 'two moving', volume: 300, profit: 102.3, stopped: 'null'}
  // ];
   dialogTitle: string;
   dialogText: any;
   tradeInfos: any;
   strategyID: number
   //@Output() submitClicked = new EventEmitter<any>();
   dataSource = new MatTableDataSource<TradeInfo>(this.tradeInfos);
   tradeInfo;
   intervalId;
  constructor(
    public dialogRef: MatDialogRef<PopupComponent>,
    public infoService: GetTradeInfoService,
    @Inject(MAT_DIALOG_DATA) public data: any

  ) {}


  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  ngOnInit() {

  //  this.dataSource.data = this.tradeInfo;
  
    this.dialogTitle = this.data.dialogTitle.split(' ').slice(0,2).join('+');
    this.strategyID = this.data.dialogTitle.split(' ').slice(2,3);
   
    //console.log("strategy is: " + this.strategyID);
    this.tradeInfos = this.data.dialogText;
    this.dataSource.data = this.tradeInfos;
    //console.log("pop up: " + this.dataSource + " " + this.dataSource.data.length);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
    this.intervalId = setInterval(() => this.getMoreInfo(), 5000);
    // console.log("this intervald starts");
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  closeDialog() {
    this.dialogRef.close();
    clearInterval(this.intervalId);
   // console.log("this interval: " + this.intervalId);
    //console.log(this.id)
  }


  getMoreInfo() {
    let dialogRef;
  //  console.log ("This is " + id + " trade");
    this.infoService.getTradeInfo(this.strategyID).subscribe(data => {
      if (data.length == undefined ) {
        this.tradeInfo.push(data);
      }else {
       this.tradeInfo = data;
       this.tradeInfo.reverse();
      }
      this.dataSource.data = [...this.tradeInfo];
    
    //  console.log("datasource: " + this.dataSource.data);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    });
  }



}
