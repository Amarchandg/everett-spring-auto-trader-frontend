import { Component, OnInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource, MatTable } from '@angular/material/table';
import { TradeInfoSummary } from '../../model/tradeInfoSummary';
import { GetTradeInfoService } from 'src/app/service/get-trade-info.service';
import { MatDialog } from '@angular/material/dialog';
import { PopupComponent } from './popup/popup.component';
import { TwoMovingService } from 'src/app/service/two-moving-service.service';
import { DatePipe } from '@angular/common';
import { MatSort } from '@angular/material/sort';

@Component({
  selector: 'app-view',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.css']
})
export class ViewComponent implements OnInit {

   displayedColumns: string[] = ['More Info', 'stockName', 'currentPosition', 'size', 'exitProfitLoss', 'profit', 'stopped'];
  // tradeInfoSummary: any[] = [
  //   {id: '1', stockName: 'apl', volume: 300, profit: 102.3, stopped: 'null'}
  //  ];
   tradeInfoSummary= []; 
   tradeInfo = []; 
   intervalId;
   strategyData = [];
   totalProfit: number;
   dataSource = new MatTableDataSource<TradeInfoSummary>(this.tradeInfoSummary);
   
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;
 // @ViewChild(MatTable, {static: true}) table: MatTable<any>;
  constructor(private infoService: GetTradeInfoService,
              private dialog: MatDialog,
              private ts: TwoMovingService,
              public datepipe: DatePipe,
              public changeDetectorRefs: ChangeDetectorRef) {}
   
  ngOnInit() {
    
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
    this.intervalId = setInterval(() => this.getStrategyInfo(), 5000);
    this.getStrategyInfo();
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  //Observable.interval(5000).takeWhile(() => true).subscribe(() => this.getStrategyInfo());
 

ngOnDestroy() {
  clearInterval(this.intervalId);
}

  getStrategyInfo () {
    //console.log("view getStrategy before subscribe");
    this.totalProfit = 0;
    this.infoService.getStrategyInfo().subscribe(data => {
      //  console.log(data);
      //  console.log(this.dataSource.data);
       if (data.length == undefined ) {
         this.tradeInfoSummary.push(data);
       }else {
        this.tradeInfoSummary = data;
        //this.tradeInfoSummary.reverse();
       }
       
       // calculate the total profit
       for(let i = 0; i<this.tradeInfoSummary.length; i ++) {
        this.totalProfit += this.tradeInfoSummary[i].profit;
       }

       // reorganize the data we want to show on the table
       this.reorganizeData();
      
       
     },  
     error => {
       console.log ("We got errors: " + error);
     });
    // this.changeDetectorRefs.detectChanges();
    //console.log(this.table);
  //   console.log("view getStrategy after subscribe");
  }

  getMoreInfo(id: number) {
    let dialogRef;
    let numOfStrategy = this. tradeInfoSummary.length; // get the number of strategy
  //  console.log ("This is " + id + " trade");
    this.infoService.getTradeInfo(id).subscribe(data => {
      if (data.length == undefined ) {
        this.tradeInfo.push(data);
      }else {
       this.tradeInfo = data;
      }
      
      dialogRef = this.dialog.open(PopupComponent, {
        width:'800px',
        height:'600px',
        data: {dialogTitle: "Trade Information " + id ,
               dialogText: this.tradeInfo}
      });
      this.tradeInfo.reverse();
      dialogRef.afterClosed().subscribe(result => {
        console.log(`Dialog result: ${result}`); // Pizza!
      });
      
    },  
      error => {
        console.log ("We got errors: " + error);
      });
   
  }

  stopStrategy(id: number) {
    let numOfStrategy = this. tradeInfoSummary.length; // get the number of strategy
    //console.log("You stopped " + id + " strategy");
    if(confirm("Are you sure to stop the strategy? ")) {    
      this.ts.stopStrategy(id).subscribe(data => { 
        let date = new Date();
        let date_correct = this.datepipe.transform(date, 'yyyy-MM-dd');
        // let strategy = this.tradeInfoSummary.filter(
        //   x => x.id == id
        // )[0]
        // strategy.stopped = date_correct;
        this.dataSource.data[numOfStrategy-id].stopped = this.tradeInfoSummary[numOfStrategy-id].stopped = date_correct;
      },  
      error => {
        console.log ("We got errors: " + error);
      })

    } // end of if confirm
  } // end of stopStrategy

  reorganizeData () {
    this.strategyData = [];
    for (let i = 0; i < this.tradeInfoSummary.length; i++) {
      this.strategyData[i] = {
        "id": this.tradeInfoSummary[i].id,
        "currentPosition": this.tradeInfoSummary[i].currentPosition <= 0 ? "Close" : "Open",
        "stockName": this.tradeInfoSummary[i].stock.ticker,
        "size": this.tradeInfoSummary[i].size,
        "exitProfitLoss": this.tradeInfoSummary[i].exitProfitLoss,
        "profit": this.tradeInfoSummary[i].profit,
        "stopped": this.tradeInfoSummary[i].stopped === null ? "running" : "stopped on " + this.tradeInfoSummary[i].stopped
      }
    }
    this.strategyData.reverse();
    this.tradeInfoSummary.reverse();
   this.dataSource.data = [...this.strategyData];
 //  console.log("datasource: " + this.dataSource.data);
   this.dataSource.paginator = this.paginator;
   this.dataSource.sort = this.sort;
  }
 
  
} // end of ViewComponent
