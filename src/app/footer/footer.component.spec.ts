import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FooterComponent } from './footer.component';

describe('FooterComponent', () => {
  let component: FooterComponent;
  let fixture: ComponentFixture<FooterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FooterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FooterComponent);  
  });

  it('should create', () => {
    component = fixture.componentInstance;
    expect(component).toBeDefined();
  });

  it('should render footer in p tag', () => {
    const footer = fixture.debugElement.nativeElement;
    expect(footer.querySelector('p').textContent).toEqual('Created By everett 2019');
    expect(footer.querySelector('p').nextSibling.textContent).toEqual('Gustavo * Chris * Mingyue');
  });
  
});
