import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HeaderComponent } from './header.component';

describe('HeaderComponent', () => {
  let component: HeaderComponent;
  let fixture: ComponentFixture<HeaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HeaderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HeaderComponent);
  });

  it('should create', () => {
    component = fixture.componentInstance;
    expect(component).toBeDefined();
  });

  it ('should render title in h1 tag', () => {
      const header = fixture.debugElement.nativeElement;
      expect(header.querySelector('h1').textContent).toEqual('Welcome to Auto Equity Trading Platform');
  })
});
