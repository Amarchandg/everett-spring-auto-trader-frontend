import { Stock } from './stock';


export class TradeInfo {
    constructor(
        public id: number,
        public price: number,
        public size: number,
        public stock: Stock,
        public strategy_id: number,
        public tempStockTicker: string,
        public strategy: string,
        public lastStateChange: string,
        public tradeType: string,
        public state: string,
        public stockTicker: string
    )  {}
}