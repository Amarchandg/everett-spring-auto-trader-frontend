import { Stock } from './stock';


export class TradeInfoSummary {
    constructor(
        public currentPosition: number,
        public exitProfitLoss: number,
        public id: number,
        public lastTradePrice: number,
        public profit: number,
        public size: number,
        public stock: Stock,
        public stopped: string
    )  {}
}  