import { TestBed, inject } from '@angular/core/testing';
import { GetTradeInfoService } from './get-trade-info.service';


import {
    HttpClientTestingModule,
    HttpTestingController
} from '@angular/common/http/testing';
import { TradeInfoSummary } from '../model/tradeInfoSummary';
import { TradeInfo } from '../model/tradeInfo';
import { environment } from 'src/environments/environment';

describe('GetTradeInfoServiceService', () => {
  beforeEach(() => TestBed.configureTestingModule({
      imports:[
          HttpClientTestingModule
      ],
      providers: [GetTradeInfoService]
  }));

  it('should be created', () => {
    const service: GetTradeInfoService = TestBed.get(GetTradeInfoService);
    expect(service).toBeDefined();
  });

  it(
    'should get all the strategy info',
    inject(
      [HttpTestingController, GetTradeInfoService],
      (
        httpMock: HttpTestingController,
        getTradeInfoService: GetTradeInfoService
      ) => {
       const mockStrategyInfo = [
        {"currentPosition":-1,"exitProfitLoss":1,"id":1,"lastTradePrice":93.14,"profit":0,"size":200,
         "stock":{"id":1,"ticker":"AAPL"},"stopped":null},
        {"currentPosition":-1,"exitProfitLoss":2.3,"id":2,"lastTradePrice":85.2,"profit":0,"size":450,
         "stock":{"id":1,"ticker":"AAPL"},"stopped":null}
        ];
        
        getTradeInfoService.getStrategyInfo().subscribe((event: TradeInfoSummary[]) => {
            expect(event.length).toEqual(2);
            expect(event[0]).toEqual(mockStrategyInfo[0]);
            expect(event[1]).toEqual(mockStrategyInfo[1]);
        });


        const mockReq = httpMock.expectOne(environment.baseUrl + 'strategy');

        expect(mockReq.cancelled).toBeFalsy();
        expect(mockReq.request.responseType).toEqual('json');
        expect(mockReq.request.method).toEqual('GET');
        mockReq.flush(mockStrategyInfo);

        httpMock.verify();
      } //end of =>

      
    ) // end of inject
  ); // end of 2nd test: "should get all the strategy info"

  it(
    'should get all the trade info',
    inject(
      [HttpTestingController, GetTradeInfoService],
      (
        httpMock: HttpTestingController,
        getTradeInfoService: GetTradeInfoService
      ) => {
       const mockTradeInfo = [{
        "id": 1,
        "price": 93.88,
        "size": 300,
        "stock": {
          "id": 1,
          "ticker": "AAPL"
        },
        "strategy_id": 2,
        "tempStockTicker": null,
        "strategy": null,
        "lastStateChange": "2019-07-31T19:29:12",
        "tradeType": "BUY",
        "state": "WAITING_FOR_REPLY",
        "stockTicker": "AAPL"
      }]
        getTradeInfoService.getTradeInfo(1).subscribe((event: TradeInfo[]) => {
            expect(event.length).toEqual(1);
            expect(event).toEqual(mockTradeInfo);
        });


        const mockReq = httpMock.expectOne(environment.baseUrl + 'trade/1');

        expect(mockReq.cancelled).toBeFalsy();
        expect(mockReq.request.responseType).toEqual('json');

        mockReq.flush(mockTradeInfo);

         httpMock.verify();
      } //end of =>

      
    ) // end of inject
  ); // end of 3rd test: "should get all the trade info"

});
