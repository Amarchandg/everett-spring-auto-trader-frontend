import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { TradeInfoSummary } from '../model/tradeInfoSummary';
import { TradeInfo } from '../model/tradeInfo';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class GetTradeInfoService {

  constructor(private http:HttpClient) {}

  getStrategyInfo(): Observable<TradeInfoSummary[]> {
    return this.http.get<TradeInfoSummary[]>(
      environment.baseUrl + 'strategy')
  }

  
  getTradeInfo(id: number): Observable<TradeInfo[]> {
    return this.http.get<TradeInfo[]>(environment.baseUrl + 'trade/' + id)
  }


/*
   getFilms(): Observable<Film[]> {
        this.log.log('FilmService', `returning a collection of films`)
        return this.httpClient.get<Film[]>('/assets/film-list.json') 
    }
*/

}
