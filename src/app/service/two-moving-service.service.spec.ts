import { TestBed, inject } from '@angular/core/testing';

import { TwoMovingService } from './two-moving-service.service';
//import { HttpEvent, HttpEventType } from '@angular/common/http';

import {
    HttpClientTestingModule,
    HttpTestingController
} from '@angular/common/http/testing';
import { environment } from 'src/environments/environment';

describe('TwoMovingServiceService', () => {
  beforeEach(() => TestBed.configureTestingModule({
      imports:[
          HttpClientTestingModule
      ],
      providers: [TwoMovingService]
  }));

  it('should be created', () => {
    const twoMovingService: TwoMovingService = TestBed.get(TwoMovingService);
    expect(twoMovingService).toBeDefined();
  });

  it(
    'should create a strategy',
    inject(
      [HttpTestingController, TwoMovingService],
      (
        httpMock: HttpTestingController,
        twoMovingService: TwoMovingService
      ) => {
       const mockInput = {
        "stock": {
            "id": 2,
            "ticker": "BRK-A"
          },
          "size": 500,
          "exitProfitLoss": 0.01
       }; // end of mockInput

       twoMovingService.createStrategy(mockInput).subscribe(data => {
           expect(data).toEqual(mockInput);
       }); // end of subscribe

       const mockReq = httpMock.expectOne(environment.baseUrl + 'strategy');

        expect(mockReq.cancelled).toBeFalsy();
        expect(mockReq.request.responseType).toEqual('json');
        expect(mockReq.request.method).toEqual('POST');
        expect(mockReq.request.body).toEqual(
            (mockInput)
        );

        mockReq.flush(mockInput);

        httpMock.verify();
      } // end of =>
    ) // end of inject
  ); // end of should create a strategy


  it(
    'should stop a strategy',
    inject(
      [HttpTestingController, TwoMovingService],
      (
        httpMock: HttpTestingController,
        twoMovingService: TwoMovingService
      ) => {

       twoMovingService.stopStrategy(1).subscribe(data => {
           expect(data).toEqual(undefined);
       }); // end of subscribe

       const mockReq = httpMock.expectOne(environment.baseUrl + 'strategy/stop/1');

        expect(mockReq.cancelled).toBeFalsy();
        expect(mockReq.request.responseType).toEqual('json');
        expect(mockReq.request.method).toEqual('POST');
        expect(mockReq.request.body).toEqual(
            {}
        );

        httpMock.verify();
      } // end of =>
    ) // end of inject
  ); // end of should create a strategy



});
