import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { twoMoving } from '../model/twoMoving';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class TwoMovingService{

  constructor(private http:HttpClient) { }

  createStrategy(newStrategy: twoMoving) {
    return this.http.post(
      environment.baseUrl + 'strategy' ,
      newStrategy
    )
  } // end of createStrategy

  
  stopStrategy(id: number) {
    return this.http.post (environment.baseUrl + 'strategy/stop/' + id, {})
  }

} // end of class